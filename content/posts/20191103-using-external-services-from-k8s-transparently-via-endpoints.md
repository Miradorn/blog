+++
draft = true
date = 2019-11-03T20:29:56+01:00
title = ""
description = ""
slug = ""
tags = []
categories = []
externalLink = ""
series = []
+++

How to create Services/Endpoints to point to external services (e.g. cloudsql, redis) so that k8s workloads can use them as if the service was deployed in cluster.
